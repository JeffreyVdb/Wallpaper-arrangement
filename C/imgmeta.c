#include "imgmeta.h"
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

typedef unsigned char byte;

#define MAX_PICT_WIDTH 50000
#define MAX_PICT_HEIGHT 50000
#define JPG_HEADER_LEN 11

int little_endian(int bi_en)
{
    return ((bi_en & 0xFF) << 24) +
	((bi_en & 0x00FF) << 8) +
	((bi_en & 0xFF0000) >> 8) +
	((bi_en >> 24) & 0xFF);
}

void read_png(const char * path, int * width, int * height)
{
    int fh, result;
    fh = open(path, O_RDONLY, (mode_t) 0600);
    if (fh == -1) {
	close(fh);
	return;
    }

    result = lseek(fh, 16, SEEK_SET);
    if (result == -1) {
	close(fh);
	return;
    }

    // Read width and height
    read(fh, width, sizeof(int));
    read(fh, height, sizeof(int));

    // Convert to little endian
    // TODO why convert to little endian
    *width = little_endian(*width);
    *height = little_endian(*height);

    // Close the file
    close(fh);
}

// Read dimensions of jfif decoded image
bool read_jpg(const char * path, int * width, int * height)
{
    bool state = false;
    byte * block = 0;
    int fh;
    fh = open(path, O_RDONLY, (mode_t) 0600);
    if (fh == -1) {
	close(fh);
	return false;
    }

    // Start decoding jpg image
    // TODO jfif image decoding, read width and height    
    byte hbuf[JPG_HEADER_LEN];

    read(fh, hbuf, JPG_HEADER_LEN);
    if (hbuf[0] == 0xFF && hbuf[1] == 0xD8 && hbuf[2] == 0xFF && hbuf[3] == 0xE0) {	
	if (hbuf[10] != '\0') {
	    state = false;
	    goto endread;
	}

	char * hdr = (char*) hbuf + 6;
	if (strcmp(hdr, "JFIF") == 0) {
	    byte len_buffer[4];
	    int result = 0;
	    unsigned short block_len = (hbuf[4] << 8) + hbuf[5];

	    int rd_left = block_len - (JPG_HEADER_LEN - 4);
	    block = (byte*) malloc(rd_left);
	    read(fh, block, rd_left);
	    free(block);

	    for (;;) {			
		result = read(fh, len_buffer, 4);
		if (result == 0 || result == -1) {
		    state = false;
		    break;
		}
		
		block_len = (len_buffer[2] << 8) + len_buffer[3];	       
		if (len_buffer[0] != 0xFF) {
		    puts(path);
		    fprintf(stderr, "Not start of block: %X\n", len_buffer[0]);
		    state = false;
		    break;
		}

		block = (byte*) malloc(block_len - 2);
		result = read(fh, block, block_len - 2);
		if (result == 0 || result == -1) {
		    state = false;
		    break;
		}
		
		if (len_buffer[1] == 0xC0) {
		    *height = (block[1] << 8)  + block[2];
		    *width = (block[3] << 8) + block[4];
		    free(block);
		    state = true;
		    break;
		}
		
		free(block);
	    }
	}
    }
    else {
	fprintf(stderr, "Invalid jpg image\n");
    }

endread:
    // Close the file    
    close(fh);
    return state;
}

void read_dimensions(const char * path, ImageType type, int * width, int * height)
{
    switch (type) {
    case PNG:
	read_png(path, width, height);
	break;

    case JPG:
	if (!read_jpg(path, width, height)) {
	    *width = -1;
	    *height = -1;
	}
	
	break;

    default:
	break;
    }
}

picture * read_picture(const char * path)
{
    picture * pict = 0;
    char * dotpos = strrchr(path, '.');
    if (dotpos) {
	pict = (picture*) malloc(sizeof(picture));
	char * ext = dotpos + 1;
	if (strcasecmp(ext, "png") == 0) 
	    pict->type = PNG;
	else if (strcasecmp(ext, "jpg") == 0)
	    pict->type = JPG;
	else if (strcasecmp(ext, "gif") == 0)
	    pict->type = GIF;
	else
	    return 0;

	read_dimensions(path, pict->type,
			&pict->width,
			&pict->height);

	if (pict->width == -1 && pict->height == -1) {
	    free(pict);
	    pict = NULL;
	}
    }
    else {
	// TODO recognize image type, check for headers
    }
    
    return pict;
}

#include "imgmeta.h"
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <stdbool.h>
#include <sys/stat.h>

// Default variables
int verbose = 1;
char * directory = 0;

// Function prototypes
void usage();
void process_args(int argc, char ** argv);
bool is_dir(const char * path);

int main(int argc, char ** argv)
{
    process_args(argc, argv);

    // Open the directory
    if (directory) {
	picture * pict = 0;
	DIR * dir = 0;
	struct dirent * ent;
	dir = opendir(directory);

	if (dir == NULL) {
	    perror("Error while reading directory");
	    return 1;
	}

	// Read files
	char * path = 0;
	while ((ent = readdir(dir)) != NULL) {
	    path = (char*) calloc(strlen(directory) +
				  strlen(ent->d_name) + 2, (size_t) 1);

	    if (!path) {
		perror("Can't malloc");
		return 1;
	    }

       	    strcat(path, directory);
	    strcat(path, "/");
	    strcat(path, ent->d_name);
	    //printf("\t\tDirectory: %s\n", directory);
	    //printf("\t\tPath: %s\n", path);

	    if (!is_dir(path)) {
		pict = read_picture(path);	    

		if (pict) {
		    // TODO: create directory and move picture
		    char bufwidth[10], bufheight[10];
		    sprintf(bufwidth, "%d", pict->width);
		    sprintf(bufheight, "%d", pict->height);		    

		    char * dirname = (char*) calloc(strlen(bufwidth) +
						    strlen(bufheight + 2), (size_t) 1);
		    strcat(dirname, bufwidth);
		    strcat(dirname, "x");
		    strcat(dirname, bufheight);

		    // Create full path
		    char * full_path = (char*) calloc(strlen(directory) +
						      strlen(dirname) +
						      strlen(ent->d_name) + 3,
						      (size_t) 1);

		    strcat(full_path, directory);
		    strcat(full_path, "/");
		    strcat(full_path, dirname);

		    if (!is_dir(full_path)) {
			printf("Creating directory: %s\n", dirname);
			mkdir(full_path, 0777);
		    }

		    // Now move the file to the directory
		    strcat(full_path, "/");
		    strcat(full_path, ent->d_name);
		    if (rename(path, full_path) == 0) {
			printf("%s moved to %s\n\n", ent->d_name,
			       dirname);
		    }
		    
		    free(dirname);
		    free(full_path);
		    free(pict);
		}
	    }

	    free(path);	    
	}

	closedir(dir);
    }

    // Just testing..
    /*picture * pict = read_picture("/home/jeffrey/Pictures/nerd.png");
    printf("This is a %s picture\n", (pict->type == PNG) ? "png"
	   : (pict->type == JPG) ? "jpg"
	   : (pict->type == GIF) ? "gif" : "Unknown");
    printf("Width: %d\n", pict->width);
    printf("Height: %d\n", pict->height);

    free(pict);*/
    
    return 0;
}

void usage()
{
    printf("Wallpaper arrange should be used like this:\n\n");
    printf("wallpaper_arrange \"/path/to/wallpaper.png\"\n");
}

bool is_dir(const char * path)
{
    if (!path)
	return false;
    
    DIR * dir = opendir(path);
    if (dir) {
	closedir(dir);
	return true;
    }

    closedir(dir);
    return false;
}

void process_args(int argc, char ** argv)
{
    // Process arguments...
    if (argc < 2) {
	usage();
	exit(1);
    }

    // Assign directory
    directory = argv[1];

    // ...
}

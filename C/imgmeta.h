#ifndef _IMG_META_H_
#define _IMG_META_H_

typedef enum image_type ImageType;
enum image_type
{
    PNG, JPG, GIF
};

typedef struct picture
{
    int width;
    int height;
    ImageType type;
} picture;

void read_dimensions(const char * path, ImageType type, int * width, int * height);
picture * read_picture(const char * path);

#endif // _IMG_META_H_
